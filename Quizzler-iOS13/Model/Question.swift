//
//  Question.swift
//  Quizzler-iOS13
//
//  Created by demuro1 on 1/1/21.
//  Copyright © 2021 The App Brewery. All rights reserved.
//



import Foundation
struct Question{
    let text: String
    let questionType: String
    let options: [String]
    let answer: String
    
    init(q:String,t:String,o:[String],a:String){
        text = q
        questionType = t
        options = o
        answer = a
    }
    
    
}
