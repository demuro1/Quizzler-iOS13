//
//  QuizBrain.swift
//  Quizzler-iOS13
//
//  Created by demuro1 on 1/1/21.
//  Copyright © 2021 The App Brewery. All rights reserved.
//

import Foundation

struct QuizBrain{
    
//    init(q:String,t:String,c:[String],a:String){
//        text = q
//        questionType = t
//        options = c
//        answer = a
//    }
    
    
    
    let quiz = [
        Question(q: "A slug's blood is green.", t: "true or false",o:["True","False"], a: "True"),
        Question(q: "Approximately one quarter of human bones are in the feet.", t: "true or false",o:["True","False"], a: "True"),
        Question(q: "The total surface area of two human lungs is approximately 70 square metres.", t: "true or false",o:["True","False"], a: "True"),
        Question(q: "In West Virginia, USA, if you accidentally hit an animal with your car, you are free to take it home to eat.", t: "true or false",o:["True","False"], a: "True"),
        Question(q: "In London, UK, if you happen to die in the House of Parliament, you are technically entitled to a state funeral, because the building is considered too sacred a place.", t: "true or false",o:["True","False"], a: "False"),
        Question(q: "It is illegal to pee in the Ocean in Portugal.", t: "true or false",o:["True","False"], a: "True"),
        Question(q: "You can lead a cow down stairs but not up stairs.", t: "true or false",o:["True","False"], a: "False"),
        Question(q: "Google was originally called 'Backrub'.", t: "true or false",o:["True","False"], a: "True"),
        Question(q: "Buzz Aldrin's mother's maiden name was 'Moon'.", t: "true or false",o:["True","False"], a: "True"),
        Question(q: "The loudest sound produced by any animal is 188 decibels. That animal is the African Elephant.", t: "true or false",o:["True","False"], a: "False"),
        Question(q: "No piece of square dry paper can be folded in half more than 7 times.", t: "true or false",o:["True","False"], a: "False"),
        Question(q: "Chocolate affects a dog's heart and nervous system; a few ounces are enough to kill a small dog.", t: "true or false",o:["True","False"], a: "True"),
        Question(q: "Which is the largest organ in the human body?", t: "multiple choice", o: ["Heart", "Skin", "Large Intestine"], a: "Skin"),
        Question(q: "Five dollars is worth how many nickels?", t: "multiple choice", o: ["25", "50", "100"], a: "100"),
        Question(q: "What do the letters in the GMT time zone stand for?", t: "multiple choice", o: ["Global Meridian Time", "Greenwich Mean Time", "General Median Time"], a: "Greenwich Mean Time"),
        Question(q: "What is the French word for 'hat'?", t: "multiple choice", o: ["Chapeau", "Écharpe", "Bonnet"], a: "Chapeau"),
        Question(q: "In past times, what would a gentleman keep in his fob pocket?", t: "multiple choice", o: ["Notebook", "Handkerchief", "Watch"], a: "Watch"),
        Question(q: "How would one say goodbye in Spanish?", t: "multiple choice", o: ["Au Revoir", "Adiós", "Salir"], a: "Adiós"),
        Question(q: "Which of these colours is NOT featured in the logo for Google?", t: "multiple choice", o: ["Green", "Orange", "Blue"], a: "Orange"),
        Question(q: "What alcoholic drink is made from molasses?", t: "multiple choice", o: ["Rum", "Whisky", "Gin"], a: "Rum"),
        Question(q: "What type of animal was Harambe?", t: "multiple choice", o: ["Panda", "Gorilla", "Crocodile"], a: "Gorilla"),
        Question(q: "Where is Tasmania located?", t: "multiple choice", o: ["Indonesia", "Australia", "Scotland"], a: "Australia")

    ]
    
    var questionNumber = 0
    var score = 0
    var isQuestionTrueFalse = false

    mutating func checkAnswer(_ userAnswer: String) -> Bool{
        if userAnswer == quiz[questionNumber].answer {
            score += 1
            return true
        } else {
            return false
        }
    
    }

    func getScore() -> Int {
        return score
    }
    
    func getQuestionType() -> Bool {
        return isQuestionTrueFalse
    }
    
    mutating func getQuestionText() -> String{
        if quiz[questionNumber].questionType == "true or false" {
            isQuestionTrueFalse = true
        } else {
            isQuestionTrueFalse = false
        }
        return quiz[questionNumber].text
    }
    
    func getAnswers() -> [String] {
        return quiz[questionNumber].options
    }
    
    func getProgress() -> Float{
        return Float(questionNumber)/Float(quiz.count)
    }
    
    mutating func nextQuestion() {
        if questionNumber < quiz.count - 1 {
            questionNumber += 1
        } else {
            questionNumber = 0
            score = 0
        }
    }
    

}
