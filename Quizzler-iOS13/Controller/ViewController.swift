//
//  ViewController.swift
//  Quizzler-iOS13
//
//  Created by Angela Yu on 12/07/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//


// starting leson 100

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var trueButton: UIButton!
    @IBOutlet weak var falseButton: UIButton!
    @IBOutlet weak var multiButton: UIButton!
    @IBOutlet weak var progressBar: UIProgressView!
 
    var quizBrain = QuizBrain()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        
    }

    @IBAction func answerButtonPress(_ sender: UIButton) {
      
        let userAnswer = sender.currentTitle!
        let userGotItRight = quizBrain.checkAnswer(userAnswer)

        
        if userGotItRight {
            sender.backgroundColor = UIColor.green
        } else {
            sender.backgroundColor = UIColor.red
        }
        
        quizBrain.nextQuestion()
        Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(updateUI), userInfo: nil, repeats: false)
    }
    
    @objc func updateUI(){
        questionLabel.text = quizBrain.getQuestionText()
        trueButton.backgroundColor = UIColor.clear
        falseButton.backgroundColor = UIColor.clear
        multiButton.backgroundColor = UIColor.clear
        progressBar.progress = quizBrain.getProgress()
        scoreLabel.text = "Score: \(quizBrain.getScore())"
        updateAnswerText()
        
        
    }
    
    
    func updateAnswerText(){
        let answers = quizBrain.getAnswers()
        
        if quizBrain.getQuestionType(){
            multiButton.isEnabled = false
            multiButton.isHidden = true
            trueButton.setTitle(answers[0], for: UIControl.State.normal)
            falseButton.setTitle(answers[1], for: UIControl.State.normal)
        } else {
            multiButton.isEnabled = true
            multiButton.isHidden = false
            trueButton.setTitle(answers[0], for: UIControl.State.normal)
            falseButton.setTitle(answers[2], for: UIControl.State.normal)
            multiButton.setTitle(answers[1], for: UIControl.State.normal)
        }
    }
}

